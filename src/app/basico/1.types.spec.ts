import { increment } from "./1.types"

describe('Basic test 1',()=>{
    it('should be return 30',()=>{
        const res = increment(29);
        expect(res).toBe(30);
    })
})