import { Player } from "./2.classes"

describe('Basic 2 - classes',()=>{
    let player:Player;

    beforeEach(()=>{
    player = new Player();

    })
    it('Should be return 2000 LP if the player takes 2000 of damage', () =>{
        const res = player.takeDamage(2000);
        expect(res).toBe(2000);
    })

    it('Should be return 3000 LP if the player takes 1000 of damage', () =>{
        const res = player.takeDamage(1000);
        expect(res).toBe(3000);
    })
})