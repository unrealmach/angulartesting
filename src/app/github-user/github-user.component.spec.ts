import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GithubUserComponent } from './github-user.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GithubUserService } from './github-user.service';
import { GitHubUserServiceMock } from './github-user.mock.service';
import { By } from '@angular/platform-browser';

describe('GithubUserComponent - ID - components', () => {
  let component: GithubUserComponent;
  let fixture: ComponentFixture<GithubUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GithubUserComponent ],
      imports : [HttpClientTestingModule],
      // providers: [GithubUserService]
      providers:[
        //cuando se necesite llamar al servicio (provide), llamar a la clase (useClass)
        {provide:GithubUserService, useClass:GitHubUserServiceMock}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GithubUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be display no "No user data" on init component',()=>{
    const expected = 'No user data';
    expect(component.username).toBe(expected);

    const username= fixture.debugElement.query(By.css('h1'));
    expect(username.nativeElement.textContent).toBe(expected);
  })

  it('should be display the username  after click the button', ()=>{
    const expected = 'Kevin Davila Benavides';
    const btnGetUserData =
    // fixture.debugElement.nativeElement.query(By.css('button'));
    fixture.debugElement.nativeElement.querySelector('button')
    btnGetUserData.click();
    expect(component.username).toBe(expected);
    expect(component.isButtonDisabled).toBe(true);
  })
});
