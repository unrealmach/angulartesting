import { Observable, of } from "rxjs"

export class GitHubUserServiceMock{
    getUser(username:string): Observable<any> {
      return       of(
        {name: 'Kevin Davila Benavides', 
        photo: 'https://avatars.githubusercontent.com/u/56242609?v=4', 
        location: 'Perú', followers: 62});

    }
}