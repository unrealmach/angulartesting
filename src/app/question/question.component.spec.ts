import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionComponent } from './question.component';
import { By } from '@angular/platform-browser';

describe('QuestionComponent -COMPONENTS', () => {
  let component: QuestionComponent;
  let fixture: ComponentFixture<QuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('shoud be a title',()=>{
    expect(component.title).toBe('question test');
    //To find an element by css
    const title= fixture.debugElement.query(By.css('h1'));
    expect(title.nativeElement.textContent).toBe('question test');
  })  

  it('No button should be containt No text',()=>{
    //To find an element by css
    const title= fixture.debugElement.nativeElement.querySelector('.btn-no');
    expect(title.textContent).toContain('NO');
  }) 

  it('should be disable the button on Mark response',()=>{
    component.markResponse(true);
    expect(component.disabledButtons).toBe(true);
  })
});
