import { Component } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent {
  title = 'question test';
  disabledButtons = false;
  response:boolean = false;

  ngOnInit():void {}

  markResponse(response:boolean){
    this.response = response;
    this.disabledButtons = true;
  }
}
